<?php

ini_set("include_path", '/home/admuticao/php:' . ini_get("include_path") );

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$name = htmlspecialchars($_GET["name"]);
$userMail = htmlspecialchars($_GET["mail"]);
$message = htmlspecialchars($_GET["message"]);

if (empty($name) or empty($userMail) or empty($message)) {
    $result = array(
        'error' => 'Empty values',
        'success' => false,
    );
    echo(json_encode($result));
}
else {

try {
    $subject = "Las Ñipas Contacto / " . $name . " " . $userMail;
    $body = "Mensaje de " . $name . " " . $userMail . "\r\n" . $message;

    mail('PAUL@ADIN.CL', $subject, $body, 'From: lasnipas@lasnipas.com ');
    mail('KAREN.PINTO@LASNIPAS.CL', $subject, $body, 'From: lasnipas@lasnipas.com ');
    mail('LILI@ADIN.CL', $subject, $body, 'From: lasnipas@lasnipas.com ');

    $result = array(
        'error' => null,
        'success' => true,
    );

    ob_end_clean();
    http_response_code(200);
    echo(json_encode($result)); 

} catch (Exception $e) {
    $result = array(
        'error' => $e,
        'success' => false,

    );
    ob_end_clean();
    echo(json_encode($result));
}
}
?>