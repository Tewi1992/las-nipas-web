export const toParagraphsArray = (x: string) => {
  return x.split('\n').filter((p) => Boolean(p));
};
