import React from 'react';
import { css } from '@emotion/core';
import styled from '@emotion/styled';
import { Typography } from '@material-ui/core';
import { MAIN_COLOR } from 'src/consts';

const SectionHeaderContainer = styled.div`
  display: flex;
  text-transform: uppercase;
  font-weight: bold;
`;

export const SectionHeader = ({ children, ...props }) => {
  const text = children;
  const [first, second] = text.split(' ');

  return (
    <SectionHeaderContainer {...props}>
      <Typography
        variant="h4"
        css={css`
          margin: auto;
          font-weight: bold;
          transform: scale(1.5);
        `}
      >
        {!second && first}
        {second && (
          <>
            <span
              css={css`
                color: ${MAIN_COLOR};
              `}
            >
              {first}
            </span>
            {' '}
            {second}
          </>
        )}
      </Typography>
    </SectionHeaderContainer>
  );
};
