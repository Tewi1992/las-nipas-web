import React from 'React';
import styled from '@emotion/styled';
import { MAIN_COLOR } from 'src/consts';

export const Divider = styled.div`
  width: 80%;
  margin: auto;
  border-top-width: 0.2rem;
  border-top-color: ${MAIN_COLOR};
  border-top-style: solid;
  opacity: 0.4;
`;

export default Divider;
