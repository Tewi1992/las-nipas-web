import React from 'react';
import { Typography } from '@material-ui/core';
import { css } from '@emotion/core';
import Descripcion from '../../../../images/cluster-dragon/descripcion.png';
import { toParagraphsArray } from 'src/util';
import { SubsectionHeader } from './SubsectionHeader';
import { useText } from 'src/components/TextContext';

export const DescripcionSection = () => {
  const t = useText();
  return (
    <div>
      <div
        css={css`
          display: grid;
          grid-template-columns: 1fr 1fr;
          max-width: 70rem;
        `}
      >
        <div
          css={css`
            padding: 1rem;
          `}
        >
          <SubsectionHeader title={t.descripcion.title} />
          {toParagraphsArray(t.descripcion.content).map((x, i) => (
            <Typography
              key={i}
              css={css`
                font-size: 18px;
                white-space: pre-wrap;
                margin: auto;
                text-align: justify;
                margin-bottom: 1rem;
              `}
            >
              {x}
            </Typography>
          ))}
          <ul>
            {t.descripcion.today.map((x, i) => (
              <li key={i}>
                <Typography>{x}</Typography>
              </li>
            ))}
          </ul>
        </div>

        <div
          css={css`
            padding: 1rem;
            display: flex;
          `}
        >
          <img
            css={css`
              max-width: 100%;
              margin: auto;
            `}
            src={Descripcion}
          />
        </div>
      </div>
    </div>
  );
};

export default DescripcionSection;
