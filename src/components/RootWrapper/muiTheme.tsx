import { createMuiTheme, ThemeOptions } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import { MAIN_COLOR } from 'src/consts';

export const muiThemeOptions: ThemeOptions = {
  typography: {
    fontFamily: ['Alegreya Sans', 'Helvetica', 'Arial', 'sans-serif'],
  },
  palette: {
    primary: { ...green, main: MAIN_COLOR },
    background: {
      default: '#ffffff',
    },
  },
};

export const muiTheme = createMuiTheme(muiThemeOptions);

export default muiTheme;
