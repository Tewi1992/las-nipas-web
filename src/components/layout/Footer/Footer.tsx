import styled from '@emotion/styled';

export const Footer = styled.footer`
  grid-row: 3;
  grid-column: 1 / 4;
`;

export default Footer;
